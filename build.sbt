name := "cass-session"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "com.datastax.cassandra" % "cassandra-driver-core" % "3.0.0",
  "io.getquill" % "quill-core_2.11" % "0.4.0",
  "io.getquill" % "quill-jdbc_2.11" % "0.4.0",
  "io.getquill" % "quill-cassandra_2.11" % "0.4.0",
  "org.scalatest" %%"scalatest" % "2.2.6" % "test"
)

