package cass.session

import com.datastax.driver.core.{Cluster, Session}

import scala.collection.JavaConversions._

// case class Product(id: String, vendor: String, category: String, desc: String, cost: Double, name: String)

object Main extends App {

  import ProductDao._

  // createNewProduct(qProduct("moto g", "moto", "moto", "smart phones", 12000, "moto g"))

  getProductNames()
}


object ProductDao {


  import CassClient._

  def createNewProduct(product: Product) {
   /* val insertStmt =
      s"""INSERT INTO product (product_id, vendor , category , cost , description , name ) VALUES
          |( '${product.id}','${product.vendor}','${product.category}',${product.cost},'${product.desc}','${product.name}');""".stripMargin
    loanSession((session: Session) => {
      session.execute(insertStmt)
      println("Inserted")
    })*/
  }


  def getProductNames() {
    loanSession((session: Session) => {
      val res = session.execute("select * from product").toList
      for (x <- res) {
        println(x.getString("product_id"))
      }
    })
  }
}

object CassClient {

  /**
    * the values below could be externalized ...
    */
  def loanSession(cxn: Session => Unit) = {
    val cluster = Cluster.builder().addContactPoints("127.0.0.1").build()
    val session = cluster.connect("cassdemo")
    cxn(session)
    cluster.close()
  }
}